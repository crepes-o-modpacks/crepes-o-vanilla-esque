# Crepes O Vanilla-Esque

This is a minecraft modpack aiming for good performance on both client and servers.

It is mostly intended for personal use, so you may want to seek something else on [Modrinth](https://modrinth.com/modpacks) or [CurseForge](https://www.curseforge.com/minecraft/modpacks) if you want support.

For a list of mods, see [this page](https://gitlab.com/crepes-o-modpacks/crepes-o-vanilla-esque/-/wikis/1.20.1#core).

# Disclaimer

* This modpack is provided as is. No support will be provided. Any action you do is completely on your own.
